# Dentropy Ubuntu

Just a ubuntu container with a bunch of stuff installed

[Based on this project](https://gitlab.com/dentropy/freshinstall)

[Location on dockerhub](https://hub.docker.com/repository/docker/dentropy/ubuntu)
