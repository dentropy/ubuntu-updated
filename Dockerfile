FROM ubuntu:20.04
RUN apt-get update -y
RUN apt -y install python3 python3-pip python3-dev
RUN apt -y install curl
RUN apt -y install git
RUN apt -y install vim
RUN apt -y install tmux
RUN apt -y install nano
RUN apt -y install nmap
RUN apt -y install screen
RUN apt -y install net-tools
RUN apt -y install iputils-ping
# RUN apt -y install youtube-dl
RUN apt -y install htop
RUN apt -y install bsdgames
RUN apt -y install lynx
RUN pip3 install thefuck
RUN echo "eval $(thefuck --alias)" >> ~/.bashrc
ENTRYPOINT while :; do echo 'Hit CTRL+C'; sleep 1; done